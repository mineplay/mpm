package ru.mineplay.mpm.handlers;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import ru.mineplay.mpm.MPM;

public class PacketHandler {
	@SubscribeEvent
	public void onClientPacket(FMLNetworkEvent.ClientCustomPacketEvent event) {
		if (MPM.DEBUG) System.out.println(event.getPacket().channel());
		if (event.getPacket().channel().equals("addfortabs:online")) {
			try {

				String[] data = getdata(event.getPacket().payload());
				if (data.length != 0)
					MPM.INSTANCE.fullOnline = Integer.parseInt(data[0]);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private String[] getdata(ByteBuf bb) {
		ByteBuf e = bb;
		int bytesCount = e.readableBytes();
		byte[] imageBytes = new byte[bytesCount];
		for (int i = 0; i < bytesCount; ++i)
			imageBytes[i] = e.readByte();
		return new String(imageBytes).split(";");
	}
}
