package ru.mineplay.mpm;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.resources.I18n;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import ru.mineplay.mpm.gui.*;

@Mod.EventBusSubscriber
public class Event {
	@SubscribeEvent
	public static void mainMenu(GuiOpenEvent e) {
		if (e.getGui() instanceof GuiMainMenu)
			e.setGui(new CustomGuiMainMenu());

		if (e.getGui() instanceof GuiMultiplayer)
			e.setGui(new CustomGuiMultiplayer());

		if (e.getGui() instanceof GuiDisconnected) {
			GuiDisconnected gui = (GuiDisconnected) e.getGui();
			e.setGui(new CustomGuiDisconnected(gui.parentScreen, gui.reason, gui.message));
		}

		if (e.getGui() instanceof GuiConnecting)
			e.setGui(new CutsomGuiConnecting(((GuiConnecting) e.getGui()).previousGuiScreen, Minecraft.getMinecraft(), Minecraft.getMinecraft().currentServerData));

		if (e.getGui() instanceof GuiIngameMenu)
			e.setGui(new CustomGuiIngameMenu());
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public static void overrideTab(RenderGameOverlayEvent.Pre event) {
		if (event.getType() == RenderGameOverlayEvent.ElementType.PLAYER_LIST) {
			Minecraft mc = Minecraft.getMinecraft();
			event.setCanceled(true);
			GuiPlayerTabOverlayCustomThree tab = new GuiPlayerTabOverlayCustomThree(mc, new GuiIngame(mc));

			Scoreboard scoreboard = mc.world.getScoreboard();
			ScaledResolution scaledresolution = new ScaledResolution(mc);

			tab.renderPlayerlist(scaledresolution.getScaledWidth(), scoreboard, null);
		}
	}

	@SubscribeEvent
	public static void onInitGui(GuiScreenEvent.InitGuiEvent event) {
		if (event.getGui() instanceof GuiOptions)
			event.getButtonList().add(new GuiButton(46542, event.getGui().width / 2 + 5, event.getGui().height / 6 + 144 - 6, 150, 20, I18n.format("fml.menu.modoptions")));
	}

	@SubscribeEvent
	public static void onActionPerformed(GuiScreenEvent.ActionPerformedEvent event) {
		if (event.getGui() instanceof GuiOptions && event.getButton().id == 46542)
				FMLClientHandler.instance().getClient().displayGuiScreen(new net.minecraftforge.fml.client.GuiModList(event.getGui()));
	}

	@SubscribeEvent
	public static void onDisconnect(FMLNetworkEvent.ClientDisconnectionFromServerEvent event) {
		MPM.INSTANCE.fullOnline = -1;
	}
}
