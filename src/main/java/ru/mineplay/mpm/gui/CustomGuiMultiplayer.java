package ru.mineplay.mpm.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.resources.I18n;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import ru.mineplay.mpm.util.GuiButtonSome;
import ru.mineplay.mpm.util.GuiHelper;

import java.io.IOException;
import java.util.ArrayList;

public class CustomGuiMultiplayer extends GuiScreen {

	private ArrayList<MultiPlayerPane> panes = new ArrayList<>();

	@Override
	public void initGui() {
		panes.clear();
		Keyboard.enableRepeatEvents(true);
		buttonList.clear();

		CustomGuiMainMenu.savedServerList.servers.forEach(s -> panes.add(new MultiPlayerPane(CustomGuiMainMenu.savedServerList.servers.indexOf(s), s, this)));

		drawButtons();
	}

	private void drawButtons() {
		buttonList.add(new GuiButtonSome(0, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png",
				this.width / 2 - 63, this.height - 45, 126, 17, I18n.format("gui.cancel"), true));
	}

	@Override
	public void actionPerformed(GuiButton button) throws IOException {
		switch (button.id) {
			case 0:
				mc.displayGuiScreen(null);
				break;
		}
		panes.forEach(p -> p.actionPerformed(button));
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		GL11.glEnable(GL11.GL_BLEND);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GuiHelper.bindTexture("textures/gui/bg_main_menu.png");
		GuiHelper.cleanRender(0, 0, width, height, 1);

		GuiHelper.bindTexture("textures/gui/logo.png");
		GuiHelper.cleanRenderCentered(width / 2 - 66, 50, 100, 30, 1);

		GuiHelper.bindTexture("textures/gui/split.png");
		GuiHelper.cleanRenderCentered(width / 2, 50, 1, 20, 1);
		GuiHelper.drawScalledCenteredString(fontRenderer, width / 2 + 38, 43, 1.5f, 1.5f, 1, I18n.format("multiplayer.menu.text.select"), -1);
		panes.forEach(p -> p.drawScreen(mouseX, mouseY, partialTicks));
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
}
