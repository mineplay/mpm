package ru.mineplay.mpm.gui;

import net.minecraft.client.gui.*;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.TextFormatting;
import ru.mineplay.mpm.util.GuiButtonSome;
import ru.mineplay.mpm.util.GuiHelper;

import java.io.IOException;

public class CustomGuiIngameMenu extends GuiIngameMenu {
	int pause = 0;

	public CustomGuiIngameMenu() {
		pause = 20;
	}

	public void updateScreen() {
		pause++;
	}

	@Override
	public void initGui() {
		this.buttonList.clear();

		//this.buttonList.add(new GuiButtonSome(-2, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 20, height / 2 - 25, 58, 16, I18n.format("main.menu.text.spawn"), true));
		//this.buttonList.add(new GuiButtonSome(-1, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 82, height / 2 - 25, 58, 16, I18n.format("main.menu.text.home"), true));
		this.buttonList.add(new GuiButtonSome(0, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 20, height / 2 - 20, 120, 16, I18n.format("menu.returnToGame"), true));
		this.buttonList.add(new GuiButtonSome(1, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 20, height / 2, 120, 16, I18n.format("menu.options"), true));
		this.buttonList.add(new GuiButtonSome(2, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 20, height / 2 + 20, 120, 16, I18n.format("main.menu.text.lc"), true));
		this.buttonList.add(new GuiButtonSome(3, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 20, height / 2 + 40, 120, 16, I18n.format("main.menu.text.forum"), true));
		this.buttonList.add(new GuiButtonSome(4, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 20, height / 2 + 60, 120, 16, I18n.format("menu.disconnect"), true));

	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		drawRect(0, 0, width, height, Integer.MIN_VALUE);

		drawButton(mouseX, mouseY, partialTicks);
		GuiHelper.bindTexture("textures/gui/pause.png");
		GuiHelper.cleanRender(30, height / 2 - 115, 100, 70, 0);
		fontRenderer.drawStringWithShadow(I18n.format("guiingame.menu.pause.text"), 3, this.height - 22, -1);
		int sec = pause / 20;
		if ((int) Math.floor(sec / 60) == 0) {
			fontRenderer.drawStringWithShadow(TextFormatting.GOLD + "" + sec % 60 + " " + getString((int) Math.floor(sec % 60), "секунду", "секунды", "секунд"), 3, this.height - 12, -1);
		} else {
			fontRenderer.drawStringWithShadow(TextFormatting.GOLD + "" + (int) Math.floor(sec / 60) + " " + getString((int) Math.floor(sec / 60), "минуту", "минуты", "минут") + " " + sec % 60 + " " + getString((int) Math.floor(sec % 60), "секунду", "секунды", "секунд"), 3, this.height - 12, -1);
		}
	}

	public String getString(int n, String form1, String form2, String form5) {
		n = Math.abs(n) % 100;
		int n1 = n % 10;
		if (n > 10 && n < 20) {
			return form5;
		}
		if (n1 > 1 && n1 < 5) {
			return form2;
		}
		if (n1 == 1) {
			return form1;
		}
		return form5;
	}

	@Override
	public void actionPerformed(GuiButton button) throws IOException {
		switch (button.id) {
			case -2:
				mc.player.sendChatMessage("/spawn");
				break;
			case -1:
				mc.player.sendChatMessage("/home");
				break;
			case 0:
				this.mc.displayGuiScreen(null);
				this.mc.setIngameFocus();
				break;
			case 1:
				this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
				break;
			case 2:
				GuiHelper.openUrl("https://mine-play.ru/cabinet");
				break;
			case 3:
				GuiHelper.openUrl("https://mine-play.ru/forum/index.php");
				break;
			case 4:
				this.mc.world.sendQuittingDisconnectingPacket();
				this.mc.loadWorld(null);
				this.mc.displayGuiScreen(new GuiMainMenu());
				break;
		}
	}

	private void drawButton(int mouseX, int mouseY, float partialTicks) {
		for (GuiButton guiButton : this.buttonList) {
			guiButton.drawButton(this.mc, mouseX, mouseY, partialTicks);
		}

		for (GuiLabel guiLabel : this.labelList) {
			guiLabel.drawLabel(this.mc, mouseX, mouseY);
		}
	}
}
