package ru.mineplay.mpm.gui;

import net.minecraft.client.gui.*;
import net.minecraft.client.multiplayer.ServerList;
import net.minecraft.client.network.ServerPinger;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.client.FMLClientHandler;
import org.lwjgl.opengl.GL11;
import ru.mineplay.mpm.util.GuiButtonSome;
import ru.mineplay.mpm.util.GuiHelper;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.UnknownHostException;


public class CustomGuiMainMenu extends GuiMainMenu { // Этот код отстой, ты это знаешь и я это знаю. Продолжай читать, а назвать меня идиотом всегда успеешь
	public static ServerList savedServerList;
	public static void openUrl(String url) {
		try {
			Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
			if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE))
				desktop.browse(new URI(url));
		} catch (Exception ignore) {
		}
	}

	@Override
	public void initGui() {
		buttonList.clear();
		viewportTexture = new DynamicTexture(256, 256);
		backgroundTexture = mc.getTextureManager().getDynamicTextureLocation("background", viewportTexture);
		widthCopyright = fontRenderer.getStringWidth("Copyright Mojang AB.");
		widthCopyrightRest = width - widthCopyright - 2;

		addSingleplayerMultiplayerButtons(height / 4 + 48, 24);

		synchronized (threadLock) {
			openGLWarning1Width = fontRenderer.getStringWidth(openGLWarning1);
			openGLWarning2Width = fontRenderer.getStringWidth(openGLWarning2);
			int k = Math.max(openGLWarning1Width, openGLWarning2Width);
			openGLWarningX1 = (width - k) / 2;
			openGLWarningY1 = (buttonList.get(0)).y - 24;
			openGLWarningX2 = openGLWarningX1 + k;
			openGLWarningY2 = openGLWarningY1 + 24;
		}

		mc.setConnectedToRealms(false);
		FMLClientHandler.instance().setupServerList();
		savedServerList = new ServerList(this.mc);
		savedServerList.loadServerList();
		new Thread(() -> savedServerList.servers.forEach(p -> {
			try {
				new ServerPinger().ping(p);
			} catch (UnknownHostException ignore) {
			}
		})).start();
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		GL11.glEnable(GL11.GL_BLEND);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GuiHelper.bindTexture("textures/gui/bg_main_menu.png");
		GuiHelper.cleanRender(0, 0, width, height, 1);

		this.drawString(this.fontRenderer, "Copyright Mojang AB", this.widthCopyrightRest, this.height - 10, -1);

		GuiHelper.bindTexture("textures/gui/logo.png");
		GuiHelper.cleanRenderCentered(90, height / 2 - 76, 110, 32, 1); //100, 30

		GuiHelper.bindTexture("textures/gui/votebarbg.png");
		GuiHelper.cleanRender(width - 250, height / 2 - 50, 240, 100, 2);
		drawButton(mouseX, mouseY, partialTicks);

		GuiHelper.drawScalledString(fontRenderer, width - 244, height / 2 - 44, 1.01f, 1.05f, 2, I18n.format("main.menu.text.vote1", TextFormatting.DARK_PURPLE + mc.getSession().getProfile().getName() + TextFormatting.RESET + TextFormatting.BLACK), -1);

		for (int i = 0; i < I18n.format("main.menu.text.vote2").split("/n").length; i++)
			GuiHelper.drawScalledString(fontRenderer, width - 244, height / 2 - 25 + (10 * i), 1.0f, 1.0f, 2,
					I18n.format("main.menu.text.vote2").split("/n")[i], 0x51514F, false);
	}

	@Override
	public void addSingleplayerMultiplayerButtons(int p_73969_1_, int p_73969_2_) {
		buttonList.add(new GuiButtonSome(0, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 30, height / 2 - 48, 125, 16, I18n.format("main.menu.text.game"), true));
		buttonList.add(new GuiButtonSome(1, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 30, height / 2 - 28, 125, 16, I18n.format("main.menu.text.settings"), true));
		buttonList.add(new GuiButtonSome(2, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 30, height / 2 - 8, 125, 16, I18n.format("main.menu.text.lc"), true));
		buttonList.add(new GuiButtonSome(3, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 30, height / 2 + 12, 125, 16, I18n.format("main.menu.text.forum"), true));
		buttonList.add(new GuiButtonSome(4, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", 30, height / 2 + 32, 125, 16, I18n.format("menu.quit"), true));
		//vote button
		for (int i = 0; i < 3; i++)
			buttonList.add(new GuiButtonSome(5 + i, "textures/gui/buttons/buttonvote" + i + ".png",
					"textures/gui/buttons/buttonvote" + i + ".png", width - 244 + (i * 45), height / 2 + 6, 40, 15, i + "", false));
		buttonList.add(new GuiButtonSome(9, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", width - 243, height / 2 + 25, 110, 16, I18n.format("main.menu.text.getbonus"), true));
		buttonList.add(new GuiButtonSome(10, "textures/gui/buttons/btn.png", "textures/gui/buttons/btn-sel.png", width - 243 + 115, height / 2 + 25, 110, 16, I18n.format("main.menu.text.topbonus"), true));

	}

	private void drawButton(int mouseX, int mouseY, float partialTicks) {
		for (GuiButton guiButton : this.buttonList) {
			guiButton.drawButton(this.mc, mouseX, mouseY, partialTicks);
		}

		for (GuiLabel guiLabel : this.labelList) {
			guiLabel.drawLabel(this.mc, mouseX, mouseY);
		}
	}

	@Override
	public void actionPerformed(GuiButton button) throws IOException {
		switch (button.id) {
			case 0:
				this.mc.displayGuiScreen(new GuiMultiplayer(this));
				break;
			case 1:
				this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
				break;
			case 2:
				openUrl("https://mine-play.ru/cabinet");
				break;
			case 3:
				openUrl("https://mine-play.ru/forum/index.php");
				break;
			case 4:
				this.mc.shutdown();
				break;
			case 5:
				openUrl("https://topcraft.ru/servers/10062/?voting=10062");
				break;
			case 6:
				openUrl("http://mcrate.su/rate/4837");
				break;
			case 7:
				openUrl("https://mctop.su/servers/5854/");
				break;
			/*case 8:
				openUrl("https://topcraft.ru/servers/10062/?voting=10062"); break;*/
		}
	}
}
