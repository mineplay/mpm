package ru.mineplay.mpm.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiDisconnected;
import net.minecraft.client.gui.GuiLabel;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.text.ITextComponent;
import ru.mineplay.mpm.util.GuiHelper;

import java.io.IOException;

public class CustomGuiDisconnected extends GuiDisconnected {
	public CustomGuiDisconnected(GuiScreen screen, String reasonLocalizationKey, ITextComponent chatComp) {
		super(screen, reasonLocalizationKey, chatComp);
	}

	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		GuiHelper.bindTexture("textures/gui/bg_main_menu.png");
		GuiHelper.cleanRender(0, 0, width, height, 1);
		drawRect(0, (height / 3), width, (height - height / 3), Integer.MIN_VALUE);
		drawButton(mouseX, mouseY, partialTicks);
		GuiHelper.drawScalledCenteredString(fontRenderer, width / 2, height / 2 - textHeight / 2 - fontRenderer.FONT_HEIGHT * 2, 1, 1, 3, reason, -1);
		int i = height / 2 - textHeight / 2;

		if (multilineMessage != null) {
			for (String s : multilineMessage) {
				drawCenteredString(fontRenderer, s, width / 2, i, 16777215);
				i += fontRenderer.FONT_HEIGHT;
			}
		}
	}

	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.id == 0) {
			this.mc.displayGuiScreen(null);
		}

	}

	private void drawButton(int mouseX, int mouseY, float partialTicks) {
		for (GuiButton guiButton : this.buttonList) {
			guiButton.drawButton(this.mc, mouseX, mouseY, partialTicks);
		}

		for (GuiLabel guiLabel : this.labelList) {
			guiLabel.drawLabel(this.mc, mouseX, mouseY);
		}
	}
}
