package ru.mineplay.mpm.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiLabel;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.mineplay.mpm.util.GuiHelper;

import java.io.IOException;

public class CutsomGuiConnecting extends GuiConnecting {
	public ServerData serverDataIn;

	public CutsomGuiConnecting(GuiScreen parent, Minecraft mcIn, ServerData serverDataIn) {
		super(parent, mcIn, serverDataIn);
		this.serverDataIn = serverDataIn;
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		GuiHelper.bindTexture("textures/gui/bg_main_menu.png");
		GuiHelper.cleanRender(0, 0, width, height, 1);
		drawRect(0, (this.height / 3), this.width, (this.height - this.height / 3), Integer.MIN_VALUE);
		GuiHelper.drawScalledCenteredString(fontRenderer, this.width / 2 - 20, this.height / 2 - 30, 1.2f, 1.2f, 2, I18n.format("connect.connecting"), -1);
		GuiHelper.drawScalledCenteredString(fontRenderer, this.width / 2 - 20, this.height / 2 - 10, 1.2f, 1.2f, 2, I18n.format("connect.menu.text.serverinfo",
				serverDataIn.serverName, (serverDataIn.populationInfo == null ?
						TextFormatting.RED + I18n.format("server.off") : serverDataIn.populationInfo) + TextFormatting.RESET), -1);

		drawButton(mouseX, mouseY, partialTicks);
	}

	@Override
	public void initGui() {
		this.buttonList.add(new GuiButton(0, this.width / 2 - 100,
				Math.min(this.height / 2 + this.fontRenderer.FONT_HEIGHT / 2 + this.fontRenderer.FONT_HEIGHT, this.height - 30), I18n.format("gui.cancel")));
	}

	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.id == 0) {
			this.cancel = true;
			if (this.networkManager != null) {
				this.networkManager.closeChannel(new TextComponentString("Aborted"));
			}

			this.mc.displayGuiScreen(null);
		}

	}

	private void drawButton(int mouseX, int mouseY, float partialTicks) {
		for (GuiButton guiButton : this.buttonList) {
			guiButton.drawButton(this.mc, mouseX, mouseY, partialTicks);
		}

		for (GuiLabel guiLabel : this.labelList) {
			guiLabel.drawLabel(this.mc, mouseX, mouseY);
		}
	}

}
