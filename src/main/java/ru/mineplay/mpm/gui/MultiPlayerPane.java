package ru.mineplay.mpm.gui;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.network.ServerPinger;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.client.FMLClientHandler;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.mineplay.mpm.util.GuiHelper;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.List;

public class MultiPlayerPane {
	int index, width, height;
	ServerData sdata;
	CustomGuiMultiplayer parent;
	ServerPinger pinger = new ServerPinger();
	boolean pigned = false;
	boolean click = false;
	boolean enabled = false;
	int offsetX = 0;

	public MultiPlayerPane(int index, ServerData sdata, CustomGuiMultiplayer parent) {
		this.index = index;
		this.sdata = sdata;
		this.parent = parent;
		this.width = parent.width;
		this.height = parent.height;
	}

	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		if (!pigned)
			new Thread(() -> {
				try {
					click = false;
					//pinger.ping(sdata);
					Thread.sleep(100);

				} catch (Exception ignore) {
				} finally {
					enabled = true;
					pigned = true;
				}
			}).start();
		if (isMouseOverArea(mouseX, mouseY, width / 2 - 130, height / 3 - 10 + (40 * index), 265, 35))
			animate();
		else
			offsetX = 0;

		GuiHelper.bindTexture("textures/gui/pane/pane_bg.png");
		GuiHelper.cleanRenderCentered(width / 2 + offsetX, height / 3 + 10 + (40 * index), 270, 38, 1);

		if (sdata.populationInfo == null)
			GuiHelper.bindTexture("textures/gui/pane/game_but_dis.png");
		else GuiHelper.bindTexture("textures/gui/pane/game_but.png");

		GuiHelper.cleanRenderCentered(width / 2 + 108 + offsetX, height / 3 + 9 + (40 * index), 12, 12, 1);


		if (isMouseOverArea(mouseX, mouseY, width / 2 - 130 + offsetX, height / 3 - 10 + (40 * index), 265, 35)
				&& Mouse.isButtonDown(0)
				&& !click
				&& enabled) {
			click = true;
			FMLClientHandler.instance().connectToServer(new GuiMultiplayer(null), sdata);
		}

		if (sdata.getBase64EncodedIconData() != null) {
			TextureUtil.uploadTextureImage(index + 1, decodeToImage(sdata.getBase64EncodedIconData()));
			GuiHelper.cleanRenderCentered(width / 2 - 110 + offsetX, height / 3 + 9 + (40 * index), 24, 24, 1);
		}

		GuiHelper.drawScalledString(parent.fontRenderer, width / 2 - 85 + offsetX, height / 3 + 3 + (40 * index), 1.25f, 1.25f, 1, sdata.serverName + " " +
				(sdata.populationInfo == null
						? TextFormatting.RED + I18n.format("server.off")
						: TextFormatting.GREEN + sdata.populationInfo.replaceAll("§.", "")
				) + TextFormatting.RESET, -1);

		/*if (isMouseOverArea(mouseX, mouseY, width / 2 - 130, height / 3 -2 + (40 * index), 25, 25) && sdata.populationInfo != null) {
			List l = new ArrayList();
			l.add("ping: " + sdata.pingToServer);
			parent.drawHoveringText(l,width / 2 - 110, height / 3 +9 + (40 * index));
		}*/
	}

	private void animate() {
		new Thread(() -> {
			try {
				while (offsetX <= 20) {
					offsetX++;
					Thread.sleep(100);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}

	private boolean isMouseOverArea(int mouseX, int mouseY, int posX, int posY, int sizeX, int sizeY) {
		return (mouseX >= posX && mouseX < posX + sizeX && mouseY >= posY && mouseY < posY + sizeY);
	}

	public void actionPerformed(GuiButton button) {
		if (button.id == index + 1)
			FMLClientHandler.instance().connectToServer(null, sdata);
	}

	public void ping() {
		new Thread(() -> {
			try {
				click = false;
				pinger.ping(sdata);
			} catch (Exception ignore) {
			} finally {
				pigned = true;
			}
		}).start();
	}

	public BufferedImage decodeToImage(String imageString) {
		BufferedImage image = null;
		byte[] imageByte;
		try {
			BASE64Decoder decoder = new BASE64Decoder();
			imageByte = decoder.decodeBuffer(imageString);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return image;
	}

	protected void drawHoveringTexts(List list, int xCoord, int yCoord, FontRenderer font) { //Задел на будущее
		if (!list.isEmpty()) {
			GL11.glDisable(32826);
			GL11.glDisable(2929);
			int k = 0;
			Iterator iterator = list.iterator();

			int k2;
			while (iterator.hasNext()) {
				String j2 = (String) iterator.next();
				k2 = font.getStringWidth(j2);
				if (k2 > k) {
					k = k2;
				}
			}

			int var15 = xCoord + 12;
			k2 = yCoord - 12;
			int i1 = 8;
			if (list.size() > 1) {
				i1 += 2 + (list.size() - 1) * 10;
			}

			if (var15 + k > parent.width) {
				var15 -= 28 + k;
			}

			if (k2 + i1 + 6 > parent.height) {
				k2 = parent.height - i1 - 6;
			}

			parent.zLevel = 300.0F;
			int j1 = -267386864;
			parent.drawGradientRect(var15 - 3, k2 - 4, var15 + k + 3, k2 - 3, j1, j1);
			parent.drawGradientRect(var15 - 3, k2 + i1 + 3, var15 + k + 3, k2 + i1 + 4, j1, j1);
			parent.drawGradientRect(var15 - 3, k2 - 3, var15 + k + 3, k2 + i1 + 3, j1, j1);
			parent.drawGradientRect(var15 - 4, k2 - 3, var15 - 3, k2 + i1 + 3, j1, j1);
			parent.drawGradientRect(var15 + k + 3, k2 - 3, var15 + k + 4, k2 + i1 + 3, j1, j1);
			int k1 = 1347420415;
			int l1 = (k1 & 16711422) >> 1 | k1 & -16777216;
			parent.drawGradientRect(var15 - 3, k2 - 3 + 1, var15 - 3 + 1, k2 + i1 + 3 - 1, k1, l1);
			parent.drawGradientRect(var15 + k + 2, k2 - 3 + 1, var15 + k + 3, k2 + i1 + 3 - 1, k1, l1);
			parent.drawGradientRect(var15 - 3, k2 - 3, var15 + k + 3, k2 - 3 + 1, k1, k1);
			parent.drawGradientRect(var15 - 3, k2 + i1 + 2, var15 + k + 3, k2 + i1 + 3, l1, l1);

			for (int i2 = 0; i2 < list.size(); ++i2) {
				String s1 = (String) list.get(i2);
				font.drawStringWithShadow(s1, var15, k2, -1);
				if (i2 == 0) {
					k2 += 2;
				}

				k2 += 10;
			}

			parent.zLevel = 0.0F;
			GL11.glEnable(2929);
			GL11.glEnable(32826);
		}
	}
}
