package ru.mineplay.mpm.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.GuiPlayerTabOverlay;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.input.Keyboard;
import ru.mineplay.mpm.MPM;
import ru.mineplay.mpm.util.GuiHelper;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class GuiPlayerTabOverlayCustomThree extends GuiPlayerTabOverlay {
	Minecraft mc;
	int height, width, cell_in_one_column, column_count, center;
	boolean debug;
	float debug_cioc, debug_cc;
	static Thread updater;
	public GuiPlayerTabOverlayCustomThree(Minecraft mcIn, GuiIngame guiIngameIn) {
		super(mcIn, guiIngameIn);
		mc = mcIn;
		debug = Keyboard.isKeyDown(Keyboard.KEY_X) && mc.player.isSneaking() || MPM.DEBUG;

		ScaledResolution scaledresolution = new ScaledResolution(mc);
		height = scaledresolution.getScaledHeight();
		width = scaledresolution.getScaledWidth();
		NetHandlerPlayClient nethandlerplayclient = mc.player.connection;
		List<NetworkPlayerInfo> list = new ArrayList<>(nethandlerplayclient.getPlayerInfoMap());

		debug_cioc = (height - 58 * 2) / 13.7f;
		cell_in_one_column = (int) debug_cioc;

		debug_cc = ((width - 25 * 2) / 100f);
		column_count = (int) Math.min(debug_cc,
				Math.ceil((float) list.size() / cell_in_one_column));

		center = width / 2 - 25;
	}
	@Override
	public void renderPlayerlist(int width, Scoreboard scoreboardIn, @Nullable ScoreObjective scoreObjectiveIn) {
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GuiHelper.bindTexture("textures/gui/tab/bg.png");

		GuiHelper.cleanRender(width / 2 - column_count * 72, 25, column_count * 144, height - 50, 1);

		if (!debug) GuiHelper.drawScalledCenteredString(mc.fontRenderer, (width - 25) / 2 - 7, 33, 1.4f, 1.4f, 2,
				"Ты играешь на " + TextFormatting.GREEN + "Mine-Play.ru", -1, false);

		GuiHelper.bindTexture("textures/gui/tab/split.png");
		GuiHelper.cleanRenderCentered((width - 25) / 2 + 10, 54, 150, 1, 3);

		GuiHelper.bindTexture("textures/gui/tab/split.png");
		GuiHelper.cleanRenderCentered((width - 25) / 2 + 10, height - 54, 150, 1, 3);

		NetHandlerPlayClient nethandlerplayclient = mc.player.connection;
		int online = MPM.INSTANCE.fullOnline == -1 ? nethandlerplayclient.getPlayerInfoMap().size() : MPM.INSTANCE.fullOnline;
		int ping = nethandlerplayclient.getPlayerInfo(mc.player.getName()) == null ? -1 : nethandlerplayclient.getPlayerInfo(mc.player.getName()).getResponseTime();
		GuiHelper.drawScalledCenteredString(mc.fontRenderer, (width - 25) / 2 + 8, height - 45, 1f, 1f, 3,
				String.format("Онлайн: %s  Пинг: %s", TextFormatting.GREEN + "" + online + TextFormatting.RESET, coloredPing(ping)), -1);
		drawSelfPing(mc.getCurrentServerData().pingToServer, (width - 25) / 2 + 56, height - 42);
		drawPlayers(nethandlerplayclient);
		GlStateManager.disableBlend();
	}

	private void drawPlayers(NetHandlerPlayClient nethandlerplayclient) {
		try {
			List<NetworkPlayerInfo> list = ENTRY_ORDERING.sortedCopy(nethandlerplayclient.getPlayerInfoMap());
			int startx = width / 2 - (column_count) * 50;
			int starty = 58;

			int x_offset = 52;
			int y_offset = 14;
			for (NetworkPlayerInfo player : list) {

				int startx_local;
				int index = list.indexOf(player);
				if (index < column_count * cell_in_one_column) {

					int local_position = index % cell_in_one_column;

					startx_local = startx + (index / cell_in_one_column) * x_offset;

					int localx = (int) (startx_local + Math.floor(index / cell_in_one_column) * x_offset);
					int localy = starty + (local_position * y_offset);

					//cell bg
					GlStateManager.pushMatrix();
					GlStateManager.translate(0, 0, 2);
					Gui.drawRect(localx, localy,
							localx + 96, localy + 12, Integer.MIN_VALUE);
					GlStateManager.popMatrix();

					//name
					GuiHelper.drawScalledString(mc.fontRenderer, localx + 16, localy + 1, 1, 1, 2, getPlayerName(player), -1, false);

					//ping
					drawPlayerPing(localx + 30, 65, localy + 1.5f, player);

					//skin
					if (player.getLocationSkin() != null) {
						mc.getTextureManager().bindTexture(player.getLocationSkin());
						GuiHelper.renderSkinHead(localx, localy);
					}

				}
			}
			if (debug) {
				GuiHelper.drawScalledString(width / 2 - 65, 25, 1, 1, "Debug mode, verion: " + MPM.VERSION, -1);

				GuiHelper.drawScalledString(width / 2 - 65, 35, 1, 1, "column_count: " + column_count + " (width: " + width + ")", -1);
				GuiHelper.drawScalledString(width / 2 - 65, 45, 1, 1, "cell_in_one_column: " + cell_in_one_column + " (height: " + height + ")", -1);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String coloredPing(long ping) {
		TextFormatting j;
		if (ping < 150) j = TextFormatting.GREEN;
		else if (ping < 300) j = TextFormatting.YELLOW;
		else if (ping < 600) j = TextFormatting.GOLD;
		else if (ping < 1000) j = TextFormatting.RED;
		else j = TextFormatting.DARK_RED;
		return j + (ping + "");
	}

	private void drawSelfPing(long ping, int x, int y) {
		if (ping < 150) GuiHelper.bindTexture("textures/gui/tab/ping_green.png");
		else if (ping < 300) GuiHelper.bindTexture("textures/gui/tab/yellow.png");
		else if (ping < 600) GuiHelper.bindTexture("textures/gui/tab/ping_gold.png");
		else GuiHelper.bindTexture("textures/gui/tab/ping_red.png");

		GuiHelper.cleanRenderCentered(x, y, 11, 9, 3);
	}

	private void drawPlayerPing(int p_175245_1_, int p_175245_2_, float p_175245_3_, NetworkPlayerInfo networkPlayerInfoIn) {
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(Gui.ICONS);
		int j;

		if (networkPlayerInfoIn.getResponseTime() < 0) j = 5;
		else if (networkPlayerInfoIn.getResponseTime() < 150) j = 0;
		else if (networkPlayerInfoIn.getResponseTime() < 300) j = 1;
		else if (networkPlayerInfoIn.getResponseTime() < 600) j = 2;
		else if (networkPlayerInfoIn.getResponseTime() < 1000) j = 3;
		else j = 4;

		zLevel += 100.0F;
		drawTexturedModalRect(p_175245_2_ + p_175245_1_ - 11, p_175245_3_, 0, 176 + j * 8, 10, 8);
		zLevel -= 100.0F;
	}
}
