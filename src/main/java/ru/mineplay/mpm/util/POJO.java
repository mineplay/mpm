package ru.mineplay.mpm.util;

public class POJO {
	public String text;
	/**
	 * 0 - коины coins.png
	 * 1 - коины x2 coins2.png
	 * 2 - коины x3 coins3.png
	 * 3 - коины +20 coins20.png
	 * 4 - сундук chest.png
	 * 5 - сундук с коинами chest_coins.png
	 * 6 - шапки hats.png
	 * 7 - косметика cosm.png
	 * 8 - маски mask.png
	 * 9 - животные pet.png
	 * 10 - рандом random.png
	 */
	public int bg, xoffset, yoffset, xOffsetScalled, yOffsetScalled, textx, texty;

	public POJO(String text, int bg) {
		this.text = text;
		this.bg = bg;
		this.xoffset = 0;
		this.yoffset = 0;
		this.xOffsetScalled = 0;
		this.yOffsetScalled = 0;
	}

	public POJO(String text, int bg, int xoffset, int yoffset, int xOffsetScalled, int yOffsetScalled, int textx, int texty) {
		this.text = text;
		this.bg = bg;
		this.xoffset = xoffset;
		this.yoffset = yoffset;
		this.xOffsetScalled = xOffsetScalled;
		this.yOffsetScalled = yOffsetScalled;
		this.textx = textx;
		this.texty = texty;
	}

	public POJO(String text, int bg, int xoffset, int yoffset, int xOffsetScalled, int yOffsetScalled) {
		this.text = text;
		this.bg = bg;
		this.xoffset = xoffset;
		this.yoffset = yoffset;
		this.xOffsetScalled = xOffsetScalled;
		this.yOffsetScalled = yOffsetScalled;
		this.textx = 0;
		this.texty = 0;
	}

	public String[] getTextWithCarriage() {
		return text.split("#r");
	}

	public String getbg() {
		switch (bg) {
			case 0:
				return "coins.png";
			case 1:
				return "coins2.png";
			case 2:
				return "coins3.png";
			case 3:
				return "coins20.png";
			case 4:
				return "chest.png";
			case 5:
				return "chest_coins.png";
			case 6:
				return "hats.png";
			case 7:
				return "cosm.png";
			case 8:
				return "mask.png";
			case 9:
				return "pet.png";
			case 10:
				return "random.png";
			default:
				return "random.png";
		}
	}
}
