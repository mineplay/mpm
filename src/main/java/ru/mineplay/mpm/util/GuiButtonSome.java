package ru.mineplay.mpm.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import ru.mineplay.mpm.MPM;

public class GuiButtonSome extends GuiButton {
	public static GuiButtonSome Instance;
	public String imgPath = null, imgSelectedPath = null;
	int h = 0, w = 0;
	int textx, texty;
	private String text;
	private float scalledX = 1, scalledY = 1, zLevel = 2;
	private boolean drawText;

	public GuiButtonSome(int buttonId, String path, String selectedPath, int x, int y, int widthIn, int heightIn, float scalledX, float scalledY, float zLevel, String buttonText) {
		this(buttonId, path, selectedPath, x, y, widthIn, heightIn, buttonText, true);
		this.scalledX = scalledX;
		this.scalledY = scalledY;
		this.zLevel = zLevel;
	}

	public GuiButtonSome(int buttonId, String path, String selectedPath, int x, int y, int widthIn, int heightIn, String buttonText) {
		this(buttonId, path, selectedPath, x, y, widthIn, heightIn, buttonText, false);
	}

	public GuiButtonSome(int buttonId, String path, String selectedPath, int x, int y, int widthIn, int heightIn, String buttonText, boolean drawText) {
		super(buttonId, x, y, widthIn, heightIn, buttonText);
		h = heightIn;
		w = widthIn;
		imgPath = MPM.MODID + ":" + path;
		imgSelectedPath = MPM.MODID + ":" + selectedPath;
		text = buttonText;
		this.drawText = drawText;
		Instance = this;
	}

	public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
		if (this.visible) {
			FontRenderer fontrenderer = mc.fontRenderer;
			mc.getTextureManager().bindTexture(new ResourceLocation(imgPath));
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
			if (this.hovered)
				if (!imgSelectedPath.equals(MPM.MODID + ":null"))
					mc.getTextureManager().bindTexture(new ResourceLocation(imgSelectedPath));
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			draw();
			this.mouseDragged(mc, mouseX, mouseY);
			if (drawText)
				GuiHelper.drawScalledCenteredString(fontrenderer, this.x + this.width / 2, this.y + (this.height - 8) / 2, scalledX, scalledY, zLevel, text, 0xFFFFFF);

		}
	}

	public void changeText(String text) {
		this.text = text;
	}

	public void changeScalledX(float x) {
		scalledX = x;
	}

	public void changeScalledY(float y) {
		scalledY = y;
	}

	public void changeZLevel(float z) {
		zLevel = z;
	}

	public void changeTextXOffset(int x) {
		textx = x;
	}

	public void changeTextYOffset(int y) {
		textx = y;
	}

	public boolean compareString(String text) {
		return this.text.equals(text);
	}

	public void setDrawText(boolean set) {
		drawText = set;
	}

	private void draw() {
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuffer();
		bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX);
		bufferbuilder.pos(x, y + h, 0).tex(0, 1).endVertex();
		bufferbuilder.pos(x + w, y + h, 0).tex(1, 1).endVertex();
		bufferbuilder.pos(x + w, y, 0).tex(1, 0).endVertex();
		bufferbuilder.pos(x, y, 0).tex(0, 0).endVertex();
		tessellator.draw();
	}
}
