package ru.mineplay.mpm;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.FMLEventChannel;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import ru.mineplay.mpm.handlers.PacketHandler;

@Mod(
		modid = MPM.MODID,
		name = MPM.MOD_NAME,
		version = MPM.VERSION
)
public class MPM {

	public static final String MODID = "mpm";
	public static final String MOD_NAME = "MPM";
	public static final String VERSION = "1.0.2.2";
	public static final boolean DEBUG = false;
	public int fullOnline = -1;

	@Mod.Instance(MODID)
	public static MPM INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {

	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		FMLEventChannel online = NetworkRegistry.INSTANCE.newEventDrivenChannel("addfortabs:online");
		online.register(new PacketHandler());
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {

	}
}
